FROM debian:latest

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y
RUN apt-get install -y cmake g++ libgsf-1-dev git ninja-build
RUN apt-get autoremove -y  \
    && apt-get clean  \
    && apt-get autoclean

RUN command useradd --create-home --password rwc rwc

RUN mkdir /data
RUN chown -R rwc /data

RUN mkdir -p /esajpip /esajpip/cache /esajpip/log

RUN mkdir /build
WORKDIR /build
RUN git clone https://github.com/Helioviewer-Project/esajpip-SWHV.git
RUN cmake esajpip-SWHV/ -G Ninja -DCMAKE_INSTALL_PREFIX=/esajpip -DSWHV_PORT_JPIP=8090 -DSWHV_DIR_IMAGE=/data/jp2 -DSWHV_DIR_CACHE=/esajpip/cache -DSWHV_DIR_LOG=/esajpip/log
RUN ninja install

RUN chown -R root:rwc /esajpip

WORKDIR /esajpip/server/esajpip/
CMD [ "./esajpip" ]
