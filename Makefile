all: clean
	docker build -t swhv-jpip:latest -f Dockerfile .

new: clean 
	docker build --no-cache  -t swhv-jpip:latest -f Dockerfile .

clean:
	docker rmi -f swhv-jpip:latest

stop: 
	docker stop jpip

run:
	docker run  --name=jpip --rm -d -it swhv-jpip:latest
